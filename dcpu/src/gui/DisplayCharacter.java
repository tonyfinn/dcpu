package gui;
import java.awt.Color;


public class DisplayCharacter {
	
	private char character;
	private Color fg = Color.WHITE;
	private Color bg = Color.BLACK;
	
	public DisplayCharacter(int dcpuText){
		fg = getColor((short) (dcpuText >> 12));
		bg = getColor((short) ((dcpuText >> 8) & 0xf));
		character = (char) (dcpuText & 0xff);
	}
	
	public DisplayCharacter(char c){
		this.character = c;
	}
	
	public DisplayCharacter(char c, Color fg, Color bg){
		this.character = c;
		this.fg = fg;
		this.bg = bg;
	}
	
	public char getCharacter(){
		return character;
	}
	
	public Color getForegroundColor(){
		return fg;
	}
	
	public Color getBackgroundColor(){
		return bg;
	}
	
	public void setCharacter(char c){
		this.character = c;
	}
	
	public void setCharacter(int dcpuText){
		fg = getColor((short) (dcpuText >> 12));
		bg = getColor((short) ((dcpuText >> 8) & 0xf));
		character = (char) (dcpuText & 0xff);
		
	}
	
	public void setFgColor(Color fg){
		this.fg = fg;
	}
	
	public void setBgColor(Color bg){
		this.bg = bg;
	}
	
	private Color getColor(short sh){
		switch(sh){
			case 0x0: return new Color(0x000000);
			case 0x1: return new Color(0x0000aa);
			case 0x2: return new Color(0x00aa00);
			case 0x3: return new Color(0x00aaaa);
			case 0x4: return new Color(0xaa0000);
			case 0x5: return new Color(0xaa00aa);
			case 0x6: return new Color(0xaa5500);
			case 0x7: return new Color(0xaaaaaa);
			case 0x8: return new Color(0x555555);
			case 0x9: return new Color(0x5555ff);
			case 0xA: return new Color(0x55ff55);
			case 0xB: return new Color(0x55ffff);
			case 0xC: return new Color(0xff5555);
			case 0xD: return new Color(0xff55ff);
			case 0xE: return new Color(0xffff55);
			case 0xF: return new Color(0xffffff);
		}
		return Color.BLACK;
	}
	
	public String toString(){
		return "Char:" + character + " foreground:" + fg + " background:" + bg;
	}
}
