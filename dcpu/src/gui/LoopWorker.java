package gui;
import java.util.List;
import javax.swing.*;
import emulator.Emulator;
import emulator.StepResult;
import exceptions.InvalidOpCodeException;

/**
 * Class that extends Swing worker. It is used to run the emulator in a separate thread so it does not
 * lag the GUI.
 */
public class LoopWorker extends SwingWorker<Integer, StepResult>{

	private GUI gui;
	private Emulator emulator;
	
	/**
	 * Constructor for the loop worker
	 * @param gui is the parent GUI
	 * @param emulator is the emulator that is being run in this worker
	 */
	public LoopWorker(GUI gui, Emulator emulator){
		this.emulator = emulator;
		this.gui = gui;
	}
	
	public void setEmulator(Emulator emulator){
		this.emulator = emulator;
	}
	
	@Override
	/**
	 * Runs the loop
	 */
	protected Integer doInBackground(){
		while(true){
			
			try {
				StepResult sr = emulator.step();
				publish(sr);
			} catch (InvalidOpCodeException e) {
				gui.notify(e.getMessage());
				gui.reset();
				break;
			}
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				break;
			}
		}
		return 0;
		
	}
	
	@Override
	/**
	 * Updates the display when ready
	 */
	protected void process(List<StepResult> srList){
		for(StepResult sr: srList)
			gui.updateDisplay(sr);
	}
}
