package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.swing.*;
import javax.swing.text.*;

import assembler.Assembler;

import utils.FileIO;
import utils.NumUtils;
import utils.StringUtils;

import emulator.Emulator;
import emulator.StepResult;
import exceptions.AssemblerException;
import exceptions.InvalidOpCodeException;

@SuppressWarnings("serial")
/**
 * Main class that runs the program. Controls the GUI, Assemble and Emulator
 * @Author Tony Finn and Samuel Haycock
 */
public class GUI extends JFrame {

	private final static int WIDTH = 970;
	private final static int HEIGHT = 650;
	public final static boolean WLAF = true;
	
	public static boolean fileEdited = false;
	public static String currentDirectory = null;

	private JPanel mainPanel = new JPanel(); //Holds all of the panels
	
	private JScrollPane textAreaScrollPane = new JScrollPane();
	private JTextArea textEditArea = new JTextArea();
	
	private JScrollPane ramScrollPane = new JScrollPane();
	private JTable ramDisplay;
	
	private JTextPane display = new JTextPane();
	private String[] variableStringNames = {"OP : ", "A  : ","B  : ","PC : ","SP : ","EX : ","A  : ","B  : ","C  : ","X  : ","Y  : ","Z  : ","I  : ","J  : "};
	private JLabel[] variableList = new JLabel[14];
	private JButton runButton = new JButton("Run");
	private JButton stepButton = new JButton("Step");
	private JButton resetButton = new JButton("Reset");
	
	private Assembler assembler = new Assembler();
	private Emulator emulator = new Emulator();
	private LoopWorker loopWorker = new LoopWorker(this, emulator);
	
	private FileIO fio = new FileIO(this);
	private DisplayCharacter[] charList = new DisplayCharacter[512]; 
	
	//menubar objects
	private JMenuBar menubar = new JMenuBar();
    private JMenu file = new JMenu("File");
	private JMenuItem fileNew = new JMenuItem("New");
	private JMenuItem fileOpen = new JMenuItem("Open");
	private JMenuItem fileSave = new JMenuItem("Save");
	private JMenuItem fileSaveAs = new JMenuItem("Save As...");
	private JMenuItem fileExit = new JMenuItem("Exit");
	
	private boolean running = false;

	/**
	 * Constructor for the GUI
	 */
	public GUI() {
		setTitle("DCPU Simulator");
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		setLocationRelativeTo(null); //Centres the window
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		getContentPane().add(mainPanel);
		mainPanel.setLayout(null);

		menuBar();
		setUpTextArea();
		setUpDisplay();
		setUpVariableDisplay();
		setUpRAMDisplay();
		setUpActionListeners();
		setUpButtons();
		drawDisplay();
	}
	
	/**
	 * Sets the buttons position and adds them to the panel
	 */
	private void setUpButtons(){
		runButton.setBounds(522, 370, 105, 40);
		stepButton.setBounds(632, 370, 105, 40);
		resetButton.setBounds(742, 370, 105, 40);
		mainPanel.add(runButton);
		mainPanel.add(stepButton);
		mainPanel.add(resetButton);
	}
	
	/**
	 * Sets the display position and values and adds them to the panel
	 */
	private void setUpDisplay(){
		display.setBounds(520, 10, 330, 355);
		display.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 16));
		display.setEditable(false);
		display.setFocusable(false);
		for(int i = 0; i < 512; i++){
			charList[i] = new DisplayCharacter(' ');
		}
		mainPanel.add(display);
	}
	
	/**
	 * Sets the RAM display position and values and adds them to the panel
	 */
	private void setUpRAMDisplay(){
		String[][] ram = new String[0x1000][0x10];
		String[] head = new String[0x10];
		Arrays.fill(head, "0");
		for(int i = 0; i < 0x1000; i ++)
			Arrays.fill(ram[i], StringUtils.toHexString(0));
		ramDisplay = new JTable(ram, head);
		ramDisplay.setTableHeader(null);
		ramDisplay.setFocusable(false);
		ramDisplay.setEnabled(false);
		ramDisplay.setShowGrid(false);
		ramDisplay.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 16));
		ramScrollPane.getViewport().add(ramDisplay);
		ramScrollPane.setBounds(10, 420, 945, 170);
		ramScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		mainPanel.add(ramScrollPane);
	}
	
	/**
	 * Used to update the output display.
	 * Redraws the panel
	 */
	private void drawDisplay(){
		display.setText("");
		StyledDocument doc = display.getStyledDocument();
        Style style = display.addStyle("mainStyle", null);
		for(int i = 0; i < 16; i++){
			for(int j = 0; j < 32; j++){
				DisplayCharacter dc = charList[(i * 32) + j];
				StyleConstants.setForeground(style, dc.getForegroundColor());
		        StyleConstants.setBackground(style, dc.getBackgroundColor());
		        try { doc.insertString(doc.getLength(),"" + dc.getCharacter() ,style); }
		        catch (BadLocationException e){}
			}
			try { doc.insertString(doc.getLength(),"\n" ,style); }
	        catch (BadLocationException e){}
		}
	}
	
	/**
	 * Sets the edit area position and values and adds them to the panel
	 */
	private void setUpTextArea(){
		textEditArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
		textEditArea.setLineWrap(true);
		textEditArea.setWrapStyleWord(true);
		textEditArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		textAreaScrollPane.setBounds(10, 10, 500, 400);
		textAreaScrollPane.getViewport().add(textEditArea);
		textAreaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		mainPanel.add(textAreaScrollPane);
	}
	
	/**
	 * Sets the variable list position and values and adds them to the panel
	 */
	private void setUpVariableDisplay(){
		for(int i = 0; i < variableList.length; i++){
			variableList[i] = new JLabel();
			variableList[i].setFont(new Font(Font.MONOSPACED, Font.PLAIN, 16));
			variableList[i].setText(variableStringNames[i] + StringUtils.toHexString(0));
			variableList[i].setBounds(860, 10 + (i * 29), 100, 20);
			mainPanel.add(variableList[i]);
		}

	}
	
	/**
	 * Sets up all action listeners
	 */
	private void setUpActionListeners(){
		textEditArea.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent arg0) {
				if(!GUI.fileEdited){
					GUI.fileEdited = true;
				}
			}
			public void keyReleased(KeyEvent arg0) {}
			public void keyPressed(KeyEvent arg0) {}
		});
		
		fileNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newFile();
		}});
		fileSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				save();
		}});
		fileSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveAs();
		}});
		fileOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load();
		}});
		fileExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exit();
		}});
		runButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runButtonPress();
		}});
		stepButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				build();
				step();
		}});
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset();
		}});
	}
	
	/**
	 * Runs when the run button was pressed
	 * starts the loop that runs the program
	 */
	public void runButtonPress(){
		if(textEditArea.getText().equals("")){
			return;
		}
		build();
		if(running){
			runButton.setText("Run");
			running = false;
			loopWorker.cancel(true);
		} else {
			running = true;
			runButton.setText("Stop");
			loopWorker = new LoopWorker(this, emulator);
			loopWorker.execute();
		}
	}
	
	/**
	 * Steps the program once
	 */
	public void step(){
		if(!running){
			try {
				build();
				StepResult sr = emulator.step();
				updateDisplay(sr);
			} catch (InvalidOpCodeException e) {
				notify(e.getMessage());
			}
		}
	}
	
	/**
	 * Resets everything in the display
	 * clears the emulator
	 */
	public void reset(){
		int[] empty = new int[14];
		Arrays.fill(empty, 0);
		updateVariableDisplay(empty);
		loopWorker.cancel(true);
		emulator.reset();
		running = false;
		runButton.setText("Run");
		resetRamDisplay();
		for(int i = 0; i < 512; i++){
			charList[i] = new DisplayCharacter(' ');
		}
		drawDisplay();
	}
	
	/**
	 * Resets the RAM Display
	 */
	private void resetRamDisplay(){
		String[][] ram = new String[0x1000][0x10];
		String[] head = new String[0x10];
		Arrays.fill(head, "0");
		for(int i = 0; i < 0x1000; i ++)
			Arrays.fill(ram[i], StringUtils.toHexString(0));
		ramDisplay = new JTable(ram,head);
		ramDisplay = new JTable(ram, head);
		ramDisplay.setTableHeader(null);
		ramDisplay.setFocusable(false);
		ramDisplay.setEnabled(false);
		ramDisplay.setShowGrid(false);
		ramDisplay.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 16));
		ramScrollPane.getViewport().add(ramDisplay);
	}
	
	/**
	 * Assembles and loads the program if it can
	 */
	private void build(){
		if(textEditArea.getText().equals("")){
			return;
		}
		if(!emulator.isProgramLoaded()){
			List<Short> assembledCode;
			try {
				assembledCode = assembler.assemble(Arrays.asList(textEditArea.getText().split("\n")));
				for(int pos = 0; pos < assembledCode.size(); pos++){
					ramDisplay.setValueAt(StringUtils.toHexString(NumUtils.unsignedShortToInt((assembledCode.get(pos)))), pos/16, pos%16);
				}
				emulator.loadProgram(0, assembledCode);
			} catch (AssemblerException e) {
				notify(e.getMessage());
			}
		}
	}
	
	/**
	 * Updates the variable display
	 */
	private void updateVariableDisplay(int[] v){
		for(int i = 0; i < 14; i++){
			variableList[i].setText(variableStringNames[i] + StringUtils.toHexString(v[i]));
		}
	}
	
	/**
	 * Updates all of the displays
	 */
	public void updateDisplay(StepResult sr){
		Set<Integer> ramPositions = sr.getChangedRamLocations().keySet();
		
		for(Integer pos: ramPositions){
			short ramValue = sr.getChangedRamLocations().get(pos);
			ramDisplay.setValueAt(StringUtils.toHexString(NumUtils.unsignedShortToInt((ramValue))), pos/16, pos%16);
			if(sr.getDisplayChanged() && pos >= 0x8000 && pos <= 0x81ff){
				charList[pos - 0x8000].setCharacter(NumUtils.unsignedShortToInt(ramValue));
			}
		}
		
		if(sr.getDisplayChanged()){
			drawDisplay();
		}
		
		int[] v = new int[] {sr.getOpcode(), sr.getA(), sr.getB(), sr.getRegister("PC"), sr.getRegister("SP"), sr.getRegister("EX"),
                sr.getRegister("A"), sr.getRegister("B"), sr.getRegister("C"), sr.getRegister("X"),
                sr.getRegister("Y"), sr.getRegister("Z"), sr.getRegister("I"), sr.getRegister("J")};
		
		for(int i = 0; i < 14; i++){
			variableList[i].setText(variableStringNames[i] + StringUtils.toHexString(v[i]));
		}
	}
	
	/**
	 * Sets up the menu bars
	 */
	public void menuBar(){
        file.add(fileNew);
        file.add(fileOpen);
        file.add(fileSave);
        file.add(fileSaveAs);
        file.addSeparator();
        file.add(fileExit);
        menubar.add(file);

        setJMenuBar(menubar);
	}
	
	/**
	 * Called when the new button is pressed
	 * Creates a new file
	 */
	public void newFile(){
		if(!saveConfirm()) return;
		textEditArea.setText("");
		currentDirectory = null;
	}
	
	/**
	 * Called when the save button is pressed
	 * Saves the file
	 */
	public void save(){
		if(currentDirectory == null){
			saveAs();
			return;
		}
		fileEdited = false;
		fio.save(textEditArea.getText(), currentDirectory);
	}
	
	/**
	 * Called when the saveAs button is pressed
	 * Saves the file
	 */
	public void saveAs(){
		fio.save(textEditArea.getText());
		fileEdited = false;
	}
	
	/**
	 * Called when the load button is pressed
	 * loads a file
	 */
	public void load(){
		if(!saveConfirm()) return;
		String text = fio.load();
		if(text != null){
			textEditArea.setText(text);
			fileEdited = false;
		}
	}
	
	/**
	 * Called when the exit button is pressed
	 * Exits the program
	 */
	public void exit(){
		if(!saveConfirm()) return;
		System.exit(0);
	}
	
	/**
	 * If the user has edited the current file then a prompt is given asking if they would like to save
	 */
	private boolean saveConfirm(){
		if(fileEdited){
			int confirm = JOptionPane.showConfirmDialog (null, "Would You Like to Save your current project first?","Warning",JOptionPane.YES_NO_CANCEL_OPTION);
			if(confirm == JOptionPane.CANCEL_OPTION) return false;
			if(confirm == JOptionPane.YES_OPTION) save();
		}
		return true;
	}
	
	/**
	 * Notifies the user with a String
	 */
	public void notify(String message){
		JOptionPane.showMessageDialog(null,message);
	}
	
	/**
	 * main method. Runs the program
	 */
	public static void main(String[] args) {
		if (WLAF) {
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GUI gui = new GUI();
				gui.setVisible(true);
			}
		});
	}
}