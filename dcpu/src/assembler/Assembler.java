package assembler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import emulator.ParameterType;
import exceptions.AssemblerException;
import exceptions.InvalidOpCodeException;
import exceptions.InvalidStackOperationException;
import exceptions.InvalidValueException;
import exceptions.UndefinedReferenceException;


/**
 * The assembler class is responsible for taking a program in text format and 
 * converting it to a binary format that is then emulated. To use this class, 
 * convert the program to a list of strings where each string is a single line 
 * of the program. Then call the assemble(List<String> program) method, which 
 * returns a List<Short> of assembled instructions which can then be passed to the
 *  Emulator class. For example, if the assembler came across the instruction:
 *
 * SET [A], B
 *
 * it would convert it to the 16 bit value:
 *
 * 000001   01000   0001
 * B		[A]	 SET
 *
 * The method by which instructions are converted to binary is specified in the DCPU 
 * spec: http://dcpu.com/dcpu-16/.
 * 
 * @author Tony Finn and Samuel Haycock
 *
 */
public class Assembler {
	
	// The name of all standard registers without additional purposes.
	private static List<String> STANDARD_REGISTER_NAMES = 
					new ArrayList<String>();
	static {
			Collections.addAll(STANDARD_REGISTER_NAMES, "A", "B", "C", "X", "Y", "Z", "I", "J"); 
	}
	
	// The values of all standard opcodes.
	private Map<String, Short> opcodes = new HashMap<String, Short>();
	// The values of all extended opcodes.
	private Map<String, Short> extendedOpcodes = new HashMap<String, Short>();
	// The list of all symbols within the program.
	private Map<String, Short> symbols = new HashMap<String, Short>();
	
	// A regular expression to match hex numbers in the format 0xABC123...
	private static final String hexRegex = "0x[0-9a-fA-F]+";
	// A regular expression to match binary numbers in the format 0b001001110...
	private static final String binaryRegex = "0b[01]+";
	// A regular expression to match decimal numbers
	private static final String decimalRegex = "\\d+";
	// A regular expression to match all suported number types.
	private static final String numberRegex = String.format("(%s|%s|%s)", hexRegex, binaryRegex, decimalRegex);
	// A list of all references to symbols within the program.
	private Map<String, List<Short>> references;
	// The current position within the assembled program
	private short currentPos;
	
	public Assembler() {
		loadOpCodes();
	}
	
	/**
	 * Assembles a program given to the assemble as a list of lines.
	 * @param lines The lines of the program.
	 * @return A list of words that make up the assembled program.
	 * @throws InvalidOpCodeException
	 * @throws UndefinedReferenceException
	 */
	public List<Short> assemble(List<String> lines) throws AssemblerException {
		currentPos = 0;
		symbols = findSymbols(lines);
		List<Short> instructions = new ArrayList<Short>();
		
		for(int i = 0; i < lines.size(); i++) {
			List<Short> assembled_shorts = assembleLine(lines.get(i), i);
			instructions.addAll(assembled_shorts);
		}
		fixSymbolReferences(instructions);
		return instructions;
	}

	/**
	 * Changes references to symbols to the actual value of the symbol's location. 
	 * The references are stored earlier in the assembler's execution.
	 * @param program The assembled program to fix references in.
	 */
	private void fixSymbolReferences(List<Short> instructions) {
		for(String symbol: references.keySet()) {
			if(symbols.get(symbol) == -1) {
				System.out.println("Undefined reference: " + symbol);
			}
			for(short location: references.get(symbol)) {
				instructions.set(location, symbols.get(symbol));
			}
		}
	}
	
	/**
	 * Removes comments, whitespace and symbols, and records symbol locations.
	 * @param line The line to drop this content from.
	 * @param currentPos The current position in words from the start of the program.
	 * @return The code portion of the line.
	 */
	private String handleExtras(String line) {
		if(line.startsWith(":")) {
			// Drop symbols
			line = handleSymbols(line);
		}
		if(line.contains(";")) {
			line = line.split(";", 2)[0]; // Drop comments
		}

		line = line.trim();
		
		return line;
	}


	/**
	 * Removes any symbols encountered from the line and stores their location.
	 * @param line A line containing a symbol.
	 * @return The line with symbols removed.
	 */
	private String handleSymbols(String line) {
		String[] line_parts = line.split(" ", 2);
		String symbol = line_parts[0].substring(1); // Drop the :
		symbols.put(symbol, currentPos);
		if(line_parts.length < 2) {
			line = "";
		} else {
			line = line_parts[1];
		}
		return line;
	}
	
	/**
	 * Reads a number from a strign and returns it as a short. The number can be a hex number
	 * prefixed with 0x, a binary number prefixed with 0b, or a decimal number.
	 * @param data The string containing the number.
	 * @return A short representation of the number.
	 */
	private short parseNumber(String data) throws InvalidValueException {
		int result;
		if(data.startsWith("0x")) {
			result = Integer.parseInt(data.substring(2), 16);
		} else if(data.startsWith("0b")) {
			result = Integer.parseInt(data.substring(2), 2);
		} else {
			result = Integer.parseInt(data);
		}
		if(result > 0xffff) {
			throw new InvalidValueException("Invalid Value: " + result, -1);
		} else {
			return (short)result;
		}
	}
	
	/**
	 * Turns a raw data value into the values to store in the program.
	 * @param data The data to convert to a binary representation.
	 * @return A list of shorts to add to the program, one for each data value.
	 * @throws InvalidValueException 
	 */
	private List<Short> rawData(final String data) throws InvalidValueException {
		List<Short> result = new ArrayList<Short>();
		String[] parts = data.split(",");
		for(String part: parts) {
			result.add(parseNumber(part.trim()));
		}
		result.add((short)0);
		return result;
	}
	
	/**
	 * Assembles a single line of the program.
	 * @param line The line to assemble.
	 * @param lineNumber The line number of this error.
	 * @return A list of words to add to the assembled program.
	 * @throws InvalidOpCodeException
	 * @throws UndefinedReferenceException
	 */
	private String[] getParts(String line) {
		String op;
		String a;
		String b;
		
		String[] parts = line.split(" ", 2);
		op = parts[0].toUpperCase();
		if(op.equals("DAT")) {
			a = parts[1];
			b = null;
		} else {
			String[] params = parts[1].split(",");
			if(!extendedOpcodes.containsKey(op)) {
				a = params[1].trim();
				b = params[0].trim();
			} else {
				b = "";
				a = params[0].trim();
			}
		}
		return new String[] {op, a, b };
	}
	
	/**
	 * Splits a line into it's individual parts (opcode, a parameter, b parameter)
	 * @param line The line to split into parts.
	 * @return An array of the operation, a parameter and b parameter. If it is a data
	 * value on the line, op will be DAT, a will be the data values, and b will be null.
	 * @throws InvalidStackOperationException 
	 * @throws InvalidValueException 
	 */
	private List<Short> assembleLine(String line, int lineNumber) throws InvalidOpCodeException, UndefinedReferenceException, InvalidStackOperationException, InvalidValueException {
		List<Short> instructions = new ArrayList<Short>();
		line = handleExtras(line);
		if(line.equals("")) {
			// Ignore blank lines.
			return new ArrayList<Short>();
		}
		
		String[] parts = getParts(line);
		String operation = parts[0];
		String a = parts[1];
		String b = parts[2];
		
		if(operation.equals("DAT")) {
			// Not actually an operator, it's intended to store data in the program.
			return rawData(a);
		}
		
		// Find the opcode for either a normal or extended opcode.
		boolean extendedInstruction = false;
		Short instructionObj = opcodes.get(operation);
		short instruction;
		if(instructionObj == null) {
			instructionObj = extendedOpcodes.get(operation);
			if(instructionObj == null) {
				throw new InvalidOpCodeException(lineNumber);
			} else {
				extendedInstruction = true;
				instruction = (short) (instructionObj.shortValue() << 5);
			}
		} else {
			instruction = instructionObj.shortValue();
		}
		currentPos++;
		
		// Get the value for A and B
		List<Short> aCodes = getParameterCodes(a, ParameterType.A, lineNumber);
		instruction |= aCodes.get(0);
		if(aCodes.size() > 1) { instructions.add(aCodes.get(1)); currentPos++; }
		
		if(!extendedInstruction) {
			// Extended instructions only have an a parameter
			List<Short> bCodes = getParameterCodes(b, ParameterType.B, lineNumber);
			instruction |= bCodes.get(0);
			if(bCodes.size() > 1) { instructions.add(bCodes.get(1)); currentPos++; }

		}
		instructions.add(0, instruction);
		return instructions;
	}

	/**
	 * Gets the words for a parameter. The first word is the actual value of
	 * the parameter, which needs to be ORed with the opcode, while the second word (if
	 * any) is an additional word belonging after the instruction.
	 * @param param The parameter value (e.g. "[symbol]" or "9" 
	 * @param pType Whether the parameter is in the A or B position.
	 * @param lineNumber The line number of the program this instruction appeared on,
	 * for error reporting.
	 * @return A list of shorts for the parameter.
	 * @throws UndefinedReferenceException IF the line refers to a symbol that is not a register or label
	 * @throws InvalidStackOperationException IF the line tries to read from a PUSH or write to a POP
	 * @throws InvalidValueException IF a number is encountered that doesn't fit in a unsigned word.
	 */
	private List<Short> getParameterCodes(String param, ParameterType pType, int lineNumber) throws UndefinedReferenceException, InvalidStackOperationException, InvalidValueException {
		List<Short> shorts = new ArrayList<Short>();
		short value = 0;
		short nextWord = 0;
		boolean hasNextWord = false;
		if(STANDARD_REGISTER_NAMES.contains(param)) {
			// Register addresses
			value = (short)Collections.binarySearch(STANDARD_REGISTER_NAMES, param);
		} else if(symbols.containsKey(param)) {
			// Label addresses
			value = 0x1f;
			hasNextWord = true;
			nextWord = -1;
			references.get(param).add(currentPos);
		} else if(param.matches(numberRegex)) {
			// Literal numbers
			short number = parseNumber(param);
			if(number >= -1 && number <= 30 && pType == ParameterType.A) {
				value = (short) (number + 0x20);
			} else {
				value = 0x1f;
				hasNextWord = true;
				nextWord = number;
			}
		} else if(param.equals("SP")) {
			// Stack pointer
			value = 0x1b;
		} else if(param.equals("PC")) {
			// Program counter
			value = 0x1c;
		} else if(param.equals("EX")) {
			// Extension register
			value = 0x1d;
		} else if(param.equals("PUSH")) {
			// Stack push operation
			if(pType != ParameterType.B) {
				throw new InvalidStackOperationException("Unexpected PUSH", lineNumber);
			}
			value = 0x18;
		} else if(param.equals("POP")) {
			// Stack pop operation
			if(pType != ParameterType.A){
				throw new InvalidStackOperationException("Unexpected POP", lineNumber);
			}
			value = 0x18;
		} else if(param.equals("[SP]") || param.equals("PEEK")) {
			// Stack peek operation
			value = 0x19;
		} else if(param.startsWith("PICK")) {
			// Reading a specific value of the stack
			String[] parts = param.split(" ", 2);
			short n = parseNumber(parts[1]);
			value = 0x1a;
			nextWord = n;
			hasNextWord = true;
		} else if(param.matches("\\[" + numberRegex + "\\]")) {
			// Literal addresses
			String numberStr = param.substring(1, param.length() - 1);
			short number = parseNumber(numberStr);
			if(number >= -1 && number <= 30 && pType == ParameterType.A) {
				value = (short) (number + 0x20);
			} else {
				value = 0x1e;
				hasNextWord = true;
				nextWord = number;
			}
		} else if(param.matches("\\[(\\w+)\\]")) {
			// Addresses of registers or labels
			String lookup = param.substring(1, param.length() - 1);
			if(STANDARD_REGISTER_NAMES.contains(lookup)) {
				// Address at register
				value = (short)(Collections.binarySearch(STANDARD_REGISTER_NAMES, lookup) + 8);
			} else if(symbols.containsKey(lookup)) {
				// Address at label
				value = 0x1e;
				hasNextWord = true;
				nextWord = -1;
				references.get(lookup).add(currentPos);
			} else {
				throw new UndefinedReferenceException(lineNumber, lookup);
			} 
		} else {
			throw new UndefinedReferenceException(lineNumber, param);
		}
		
		// Put into correct position so that the instruction looks like
		// the folllowing bit pattern.
		// aaaaaabbbbbooooo
		if(pType == ParameterType.A) {
			value = (short) (value << 10);
		} else {
			value = (short) (value << 5);
		}
		shorts.add(value);
		if(hasNextWord) {
			shorts.add(nextWord);
		}
		return shorts;
	}
	
	/**
	 * Load all possible opcodes into the system.
	 */
	private void loadOpCodes() {
		List<String> operations = new ArrayList<String>();
		Collections.addAll(operations, "SET", "ADD", "SUB", "MUL", "MLI", "DIV", "DVI", "MOD",
			"MDI", "AND", "BOR", "XOR", "SHR", "ASR", "SHL", "IFB", "IFC", "IFE", "IFN", "IFG",
			"IFA", "IFL", "IFU", "", "", "ADX", "SBX", "", "", "STI", "STD");
		for(short code = 1; code <= 0x1f; code++) {
			String operation = operations.get(code - 1);
			if(!operation.equals("")) {
				opcodes.put(operations.get(code - 1), code);
			}
		}
		extendedOpcodes.put("JSR", (short) 0x01);
	}
	
	/**
	 * Find all symbols within the program and stores them for later.
	 * @param lines A list of all the lines within a program.
	 * @return A map of symbols to memory addresses. The shorts will later contain the 
	 * location of the symbol, but that is not done by this method.
	 */
	public Map<String, Short> findSymbols(List<String> lines) {
		Map<String, Short> symbols = new HashMap<String, Short>();
		references = new HashMap<String, List<Short>>();
		for(String line: lines) {
			line = line.trim();
			if(line.startsWith(":")) {
				String[] parts = line.split(" ");
				// Drop the :
				String symbol = parts[0].substring(1);
				symbols.put(symbol, (short)-1);
				references.put(symbol, new ArrayList<Short>());
			}
		}
		return symbols;
	}
}
