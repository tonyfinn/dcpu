package utils;


import gui.GUI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class FileIO {
	
	private GUI gui;
	private JFileChooser fc = new JFileChooser();
	
	public FileIO(GUI gui){
		this.gui = gui;
		fc.setFileFilter(new DCPUFileFilter());
	}
	
	public int save(String toSave){
		int button = fc.showSaveDialog(gui);
		if(button != 0) return -1;
		String save = fc.getSelectedFile().getPath();
		if(!save.contains(".dcpu")){
			save += ".dcpu";
		}
		return save(toSave, save);
	}
	
	public int save(String toSave, String dir){
		try {
			File file = new File(dir);
			file.createNewFile();
			FileWriter outFile = new FileWriter(file);
			PrintWriter out = new PrintWriter(outFile);
			out.write(toSave);
			out.close();
		} catch (Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public  String load(String dir) {
		StringBuilder sb = new StringBuilder();
		try {
			FileReader input = new FileReader(dir);
			BufferedReader bufRead = new BufferedReader(input);
			String line;
			while ((line = bufRead.readLine()) != null){
				if (!line.isEmpty()) {
					sb.append(line);
					sb.append("\n");
				}
			}
			bufRead.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		GUI.currentDirectory = dir;
		return sb.toString();
	}
	
	public String load(){
		int button = fc.showOpenDialog(gui);
		if(button != 0){
			return null;
		}
		return load(fc.getSelectedFile().getPath());
	}
	
}

/**
 * Inner Class to handle the file filter the allow for the custom extension of "hwca"
 */
class DCPUFileFilter extends FileFilter {

	/**
	 * Returns whether or not the given file should be accepted
	 */
	public boolean accept(File file) {
		if(file.exists()) {
			if(file.isDirectory()) {
				return true;
			}
			String ext = getExtension(file);
			if(ext.equals("dcpu")) {
				return true;
			};
		}
		return false;
	}

	/**
	 * Gets the extension of a given file
	 * @param file is the file to check
	 * @return the extension
	 */
	public String getExtension(File file) {
		String ext = "";
		if(file != null) {
			String filename = file.getName();
			int i = filename.lastIndexOf('.');
			if(i>0 && i<filename.length()-1) {
				ext = filename.substring(i+1).toLowerCase();
			};
		}
		return ext;
	}

	/**
	 * Returns the description of the file filter
	 */
	public String getDescription() {
		return "DCPU";
	}
}
