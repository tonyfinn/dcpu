package utils;

/**
 * Utility class for string related methods.
 * @author Tony
 *
 */
public class StringUtils {

	/**
	 * Convert a short to a string of bits that is padded to 16 bits long regardless
	 * of the actual value stored, and is spaced every four bits for readability.
	 * @param x The short to convert to a string.
	 * @return A string containing the binary value of the short.
	 */
	public static String toBinaryString(short x) {
		return toBinaryString(NumUtils.unsignedShortToInt(x), 16);
	}
	
	/**
	 * Converts an integer to a binary string with spaces every four digits.
	 * @param x The integer to convert to a string.
	 * @param digits The desired amount of digits in the output string.
	 * @return A string seperated with spaces every four digits. i.e toBinaryString(4, 8)
	 * 			returns 0000 0100
	 */
	public static String toBinaryString(int x, int digits) {
		String unformattedBinaryString = Integer.toBinaryString(x); 
		int paddingLength = 0;
		if(unformattedBinaryString.length() <= digits) {
			paddingLength = digits - unformattedBinaryString.length(); // Amount of 0s to prepend
		}
		
		int currentCyclePos = digits % 4; // Amount of digits since last space
		StringBuilder sb = new StringBuilder();
	 
		// Add on all the 0s
		for(int i = 0; i < paddingLength; i++) {
			sb.append('0');
			currentCyclePos++;
			if(currentCyclePos == 4) {
				sb.append(' ');
				currentCyclePos = 0;
			}
		}
		
		// Add on the actual digits of the string
		for(char c: unformattedBinaryString.toCharArray()) {
			sb.append(c);
			currentCyclePos++;
			if(currentCyclePos == 4) {
				sb.append(' ');
				currentCyclePos = 0;
			}
		}
		return sb.toString();
	}
	
	/**
	 * Convert an integer to a hex string. Note: This is only for numbers within the range of
	 * unsigned 16 bit numbers, i.e. 0x0000 - 0xffff.
	 * @param num The number to convert to a hex string.
	 * @return A String representation of the number in hexadecimal.
	 */
	public static String toHexString(int num){
		return String.format("%4s", Integer.toHexString(num & 0xffff)).replace(" ", "0");
		
	}
}
