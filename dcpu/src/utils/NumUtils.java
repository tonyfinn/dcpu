package utils;

/**
 * Utility class for numeric methods needed for DCPU emulation.
 * @author Tony
 *
 */
public class NumUtils {
	
	/**
	 * Because Java does not actually support unsigned shorts, we need to use
	 * signed shorts to represent the DCPU's unsigned words. The issue with this is casting
	 * - casting 17000 stored in 16 bits to an int will get a negative number. This method
	 * converts a short to an int as if a short was unsigned.
	 * @param x The short to convert to an integer.
	 * @return An integer of the short as an unsigned number.
	 */
	public static int unsignedShortToInt(short x) {
		return x & 0xffff;
	}

	/**
	 * Get the high 16 bits of an integer value.
	 * @param x The integer value to get the high bits from.
	 * @return The highest 16 bits of the integer.
	 */
	public static short getHighBits(int x) {
		return (short)(x >> 16);
	}
	
	/**
	 * Get the low 16 bits of an integer value.
	 * @param x The integer value to get the low bits from.
	 * @return The lowest 16 bits of the integer.
	 */
	public static short getLowBits(int x) {
		return (short)(x & 0xffff);
	}
}
