package exceptions;

/**
 * Base class for all exceptions related to the assembler.
 * @author Tony Finn and Samuel Haycock
 */
@SuppressWarnings("serial")
public class AssemblerException extends DcpuException {
	private int lineNumber;
	
	public AssemblerException() {
		this("Assembly Failed");
	}
	
	public AssemblerException(String message) {
		super(message);
		this.lineNumber = -1;
	}
	public AssemblerException(String message, int lineNumber) {
		super("Line: " + lineNumber + " - " + message);
		this.lineNumber = lineNumber;
	}
	
	public int getLineNumber() {
		return lineNumber;
	}
}
