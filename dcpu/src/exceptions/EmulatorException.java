package exceptions;

/**
 * Base class for all exception in the Emulator.
 * @author Tony Finn and Samuel Haycock
 */
@SuppressWarnings("serial")
public class EmulatorException extends DcpuException {

	public EmulatorException(String message) {
		super(message);
	}

}
