package exceptions;

@SuppressWarnings("serial")
public class InvalidOpCodeException extends AssemblerException {
	
	public InvalidOpCodeException(int lineNumber) {
		super("Invalid Opcode on line #" + lineNumber, lineNumber);
	}
	
	public InvalidOpCodeException(String message, int lineNumber) {
		super(message, lineNumber);
	}
}
