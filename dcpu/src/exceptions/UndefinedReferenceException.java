package exceptions;


@SuppressWarnings("serial")
public class UndefinedReferenceException extends AssemblerException {
	
	// TODO: Make this less stupid than having int, String constructor and String, int constructor.
	/**
	 * Create an Undefined reference exception for the specified symbol on the specified line.
	 * @param lineNumber The line number containing the undefined reference.
	 * @param symbol The symbol that is undefined.
	 */
	public UndefinedReferenceException(int lineNumber, String symbol) {
		super("Undefined reference to " + symbol + " on line #" + lineNumber, lineNumber);
	}
	
	/**
	 * Create an Undefined reference exception for the specified line.
	 * @param message The message to display.
	 * @param lineNumber The line number containing the undefined reference.
	 */
	public UndefinedReferenceException(String message, int lineNumber) {
		super(message, lineNumber);
	}
}
