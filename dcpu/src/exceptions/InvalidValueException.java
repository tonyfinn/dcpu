package exceptions;


@SuppressWarnings("serial")
public class InvalidValueException extends AssemblerException {
	public InvalidValueException(String error, int lineNumber) {
		super(error, lineNumber);
	}
}
