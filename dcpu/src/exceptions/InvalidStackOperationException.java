package exceptions;

@SuppressWarnings("serial")
public class InvalidStackOperationException extends AssemblerException {

	public InvalidStackOperationException(String message, int lineNumber) {
		super(message, lineNumber);
	}

}
