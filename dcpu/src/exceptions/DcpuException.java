package exceptions;

/**
 * Base class for all exceptions related to the Emulator or Assembler.
 * @author Tony
 *
 */@SuppressWarnings("serial")
public class DcpuException extends Exception {
	protected String message;

	public DcpuException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}