package emulator;

import java.util.Map;

/**
 * Represents the result of emulating a single instruction.
 * 
 * @author Tony Finn and Samuel Haycock
 */
public class StepResult {
	private Map<String, Short> registers;
	private Map<Integer, Short> changedRamLocations;
	private int opcode;
	private int a;
	private int b;
	private boolean displayChanged;
	
	/**
	 * Creates a new StepResult object.
	 * @param registers The values of all registers after the instruction.
	 * @param changedRamLocations The locations and new values of all modified locations in RAM.
	 * @param opcode The opcode of the instruction that was just executed.
	 * @param a The binary value of the a parameter to the instruction.
	 * @param b The binary value of the b parameter to the instruction.
	 * @param displayChanged Whether or not the display has changed and needs to be redrawn as
	 * a result of the instruction.
	 */
	public StepResult(Map<String, Short> registers,
			Map<Integer, Short> changedRamLocations, int opcode, int a, int b, boolean displayChanged) {
		this.registers = registers;
		this.changedRamLocations = changedRamLocations;
		this.opcode = opcode;
		this.a = a;
		this.b = b;
		this.displayChanged = displayChanged;
	}
	
	/**
	 * Get the value of specific register after the instruction.
	 * @param name The register to get the value of.
	 * @return The value stored in that register.
	 */
	public Short getRegister(String name) {
		return registers.get(name);
	}

	/**
	 * Get a map of all registers to their values after the instruction.
	 * @return A map of all registers to their values 
	 */
	public Map<String, Short> getRegisters() {
		return registers;
	}

	/**
	 * Get a map containing the locations and new values of all modified locations in RAM.
	 * @return The locations and new values of all modified locations in RAM.
	 */
	public Map<Integer, Short> getChangedRamLocations() {
		return changedRamLocations;
	}

	/**
	 * Get the opcode of the instruction just executed.
	 * @return The binary opcode of the operation just executed.
	 */
	public int getOpcode() {
		return opcode;
	}

	/**
	 * Get the value of the A operand to the instruction. This is the value in the assembled
	 * program and not nessecarily the value used in calculations. For example, if the
	 * instruction was SET A, [0x1000], it would return the value representing [0x1000]
	 * and not the value of the memory location 0x1000.
	 * 
	 * @return The binary value of the A operand.
	 */
	public int getA() {
		return a;
	}

	/**
	 * Get the binary value of the B operand to the instruction. This is the value in the assembled
	 * program and not nessecarily the value used in calculations. For example, if the
	 * instruction was SET A, [0x1000], it would return the value representing [0x1000]
	 * and not the value of the memory location 0x1000.
	 * @return
	 */
	public int getB() {
		return b;
	}

	/**
	 * Get whether or not the display has changed during this step.
	 * @return
	 */
	public boolean getDisplayChanged() {
		return displayChanged;
	}

}
