package emulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import utils.NumUtils;
import utils.StringUtils;
import values.DcpuValue;
import values.LiteralValue;
import values.RamValue;
import values.RegisterIndirectValue;
import values.RegisterValue;
import values.StackValue;

import commands.DcpuCommand;
import commands.JumpSetReturnCommand;
import commands.SetCommand;
import commands.bitwise.LogicalCommand;
import commands.bitwise.LogicalOperation;
import commands.bitwise.ShiftCommand;
import commands.bitwise.ShiftOperation;
import commands.conditional.ConditionalCommand;
import commands.conditional.DcpuConditional;
import commands.hardware.HardwareDevice;
import commands.hardware.HardwareNumCommand;
import commands.hardware.HardwareQueryCommand;
import commands.maths.AddCommand;
import commands.maths.AddExCommand;
import commands.maths.DivideCommand;
import commands.maths.ModCommand;
import commands.maths.MultiplyCommand;
import commands.maths.SubExCommand;
import commands.maths.SubtractCommand;

import exceptions.InvalidOpCodeException;


/**
 * The emulator class is responsible for the core of the emulation process. It reads 
 * in instructions from the virtual RAM (implemented as a short[] because it is 
 * fixed in size), breaks them into their operation and parameters, then calls 
 * run() on the correct DcpuCommand subclass for handling the operation with 
 * DcpuValue parameters that represent the parameters of the DCPU instruction. 
 * 
 * For example, if it read in the assembled instruction that represents:
 *
 * SET [A], 0x1000
 *
 * It would find its instance of SetCommand and call run() with a IndirectRegisterValue 
 * for the A register and a LiteralValue of 0x1000. 
 *
 * It is used by calling loadProgram(List<Short> program) to load in a program as a list of 
 * assembled short values, and then calling step() in a loop to run a single instruction 
 * each time. step() returns a StepResult which contains details about the instruction 
 * that ran, a map of register names to values with the values of the registers after the 
 * instruction, a map containing ram locations that changed, where the key is the address 
 * in RAM and the value is the new value of that RAM location.
 * 
 * @author Tony Finn and Samuel Haycock
 *
 */
public class Emulator {
	
	// 0000 0000 0001 1111 - the last five bits are the opcode
	private static final int OPCODE_MASK = 0x001f;
	// 1111 1100 0000 0000 - the first six bits are the A parameter.
	private static final int A_MASK = 0xfc00;
	// 0000 0011 1110 0000 - these five bits are the second parameter
	private static final int B_MASK = 0x03e0;
	
	private static final int DISPLAY_START = 0x8000;
	private static final int DISPLAY_END = 0x81ff;
	
	// The amount of bits that an instruction needs to be shifted to the right to isolate
	// the A value. (instruction >> A_SHIFT) & A_MASK gets you the A parameter.
	private static final int A_SHIFT = 10;
	// The amount of bits that an instruction needs to be shifted to the right to isolate
	// the B value. (instruction >> B_SHIFT) & B_MASK gets you the B parameter.
	private static final int B_SHIFT = 5;
	
	// The total amount of RAM in the DCPU
	private static final int WORDS_OF_RAM = 0x10000;
	
	// The register names to their value.
	private Map<String, Short> registers;
	// All valid register names.
	private String[] registerNames = {"A", "B", "C", "X", "Y", "Z", "I", "J", "PC", "SP", "EX"};
	// The memory of the emulated DCPU
	private short[] ram;

	// Map of all standard opcodes to the DcpuCommand operation to run them.
	private Map<Integer, DcpuCommand> commands;
	// Map of all extended opcodes (those with their opcode in the b position)
	// to the DcpuCommand operation to run them.
	private Map<Integer, DcpuCommand> extendedCommands;
	// RAM locations which have had their value changed this step.
	private Map<Integer, Short> changedRamLocations = new HashMap<Integer, Short>();
	// All emulated connected hardware devices
	private List<HardwareDevice> hardware = new ArrayList<HardwareDevice>();
	// All instructions that are considered to be conditional and should be skipped over
	// if a conditional evaluates to false.
	private static Set<Integer> conditionalInstructions = new HashSet<Integer>();
	static {
		// Conditional instruction opcodes from DCPU spec.
		for(int i = 0x10; i <= 0x17; i++) {
			conditionalInstructions.add(i);
		}
	}
	
	// Whether or not a program has been loaded since the last time the emulator was reset
	private boolean programLoaded = false;
	// Whether or not the display has been changed since the last step.
	private boolean displayChanged = false;
	
	/**
	 * Create a new DCPU emulator.
	 */
	public Emulator() {
		ram = new short[WORDS_OF_RAM];
		Arrays.fill(ram, (short)0);
		initRegisters();
		loadCommands();
		loadHardware();
	}
	
	/**
	 * Reset the emulator to the machine after the Dcpu was powered on.
	 */
	public void reset() {
		programLoaded = false;
		Arrays.fill(ram, (short)0);
		for(String registerName : registers.keySet()) {
			registers.put(registerName, (short)0);
		}
		registers.put("SP", (short) (WORDS_OF_RAM - 1));
	}
	
	/**
	 * Load a program into the system.
	 * @param program The program as a series of opcodes.
	 */
	public void loadProgram(List<Short> program) {
		loadProgram(0, program);
	}
	
	/**
	 * Set up the emulated connected hardware.
	 */
	private void loadHardware() {
		// The literal numbers in this method are from the 
		// spec, and in order for each call are manufacturerId, deviceId and version
		// LEM1802 http://dcpu.com/monitor/
		HardwareDevice display = new HardwareDevice(0x1c6c8b36, 0x7349f615, (short)0x1802);
		// Generic Keyboard http://dcpu.com/keyboard/
		HardwareDevice keyboard = new HardwareDevice(0, 0x30cf7406, (short) 0);
		hardware.add(display);
		hardware.add(keyboard);
	}
	
	/**
	 * Get the list of all emulated connected hardware devices.
	 * @return
	 */
	public List<HardwareDevice> getHardware() {
		return hardware;
	}
	
	/**
	 * Load a program into the system at a specified offset.
	 * @param offset The location to store the program.
	 * @param program The program as a series of opcodes.
	 */
	public void loadProgram(int offset, List<Short> program) {
		// TODO throw exception if not enough space.
		for(Short word: program) {
			ram[offset] = word;
			offset++;
		}
		programLoaded = true;
	}
	
	/**
	 * Set up all the registers for the first time.
	 */
	private void initRegisters() {
		registers = new HashMap<String, Short>();
		for(String registerName: registerNames) {
			registers.put(registerName, (short)0);
		}
		registers.put("SP", (short) (WORDS_OF_RAM - 1));
	}
	
	
	/**
	 * @return Whether or not a program has been loaded into the emulator
	 * since it was last reset.
	 */
	public boolean isProgramLoaded() {
		return programLoaded;
	}
	
	/**
	 * Gets an array containing the contents of RAM.
	 * @return An array of length WORDS_OF_RAM containing the DCPU's RAM
	 */
	public short[] getRam() {
		return Arrays.copyOf(ram, WORDS_OF_RAM);
	}
	
	/**
	 * Return a section of RAM.
	 * @param firstAddress The starting address of the RAM
	 * @param lastAddress The ending address of the RAM
	 * @return An array containing that section of RAM.
	 */
	public short[] getRam(int firstAddress, int lastAddress) {
		return Arrays.copyOfRange(ram, firstAddress, lastAddress);
	}
	
	/**
	 * Get the value stored in the specified register. 
	 * @param name The name of the register to retrieve the value of.
	 * @return The value stored in that register.
	 */
	public short getRegisterValue(String name) {
		return registers.get(name);
	}
	
	/**
	 * Set the value stored in the specified register. 
	 * @param name The name of the register to set the value of.
	 * @param value The value to store in that register.
	 */
	public void setRegisterValue(String name, short value) {
		registers.put(name, value);
	}
	
	/**
	 * Increment the value stored in a specified register by one (convenience method),
	 * wrapping around to the number of words of RAM
	 * @param name The register to increment the value of.
	 */
	public void incWrappedRegister(String name) {
		setRegisterValue(name, (short)((getRegisterValue(name) + 1) % WORDS_OF_RAM));
	}
	
	/**
	 * Decrement the value stored in a specified register by one (convenience method), 
	 * wrapping around to the number of words of RAM
	 * @param name The register to decrement the value of.
	 */
	public void decWrappedRegister(String name) {
		setRegisterValue(name, (short)((getRegisterValue(name) - 1) % WORDS_OF_RAM));
	}
	
	/**
	 * Get the value stored at the memory location whose address is stored in the given 
	 * register. For example, if register A stores the value 0x24, getValueAtRegisterValue("A")
	 * returns the value stored at memory location 0x24.
	 * @param name The name of the register to get the memory location from.
	 * @return The value stored at that memory location.
	 */
	public short getValueAtRegisterValue(String name) {
		return getRamValue(getRegisterValue(name));
	}
	
	/**
	 * Gets the value of [PC] and increments PC
	 * @return The value at the memory location referred to in the program counter.
	 */
	public short getNextInstruction() {
		short answer = getValueAtRegisterValue("PC");
		incWrappedRegister("PC");
		return answer;
	}
	
	/**
	 * Get the value stored in RAM at a given address.
	 * @param address The address as an unsigned 16 bit number.
	 * @return The value stored in that location of RAM.
	 */
	public short getRamValue(short address) {
		int location = NumUtils.unsignedShortToInt(address);
		return ram[location];
	}
	
	/**
	 * Set the value in RAM at a given address.
	 * @param address The address to store the value in as an unsigned 16 bit number.
	 * @param value The value to store in the given RAM location.
	 */
	public void setRamValue(short address, short value) {
		int location = NumUtils.unsignedShortToInt(address);
		ram[location] = value;
		changedRamLocations.put(location, value);
		if(location >= DISPLAY_START && location <= DISPLAY_END) {
			displayChanged = true;
		}
	}
	
	/**
	 * Get the correct type of value object based on the field in the instruction.
	 * @param value The value stored in a parameter (A or B in an instruction)
	 * @return The correct type of value object for reading or writing the value.
	 */
	public DcpuValue getValue(short value) {
		if(value < 0) {
			System.out.println(StringUtils.toBinaryString(value));
			return new LiteralValue(this, (short)0);
		} else if(value <= 0x07) {
			return new RegisterValue(this, registerNames[value]);
		} else if(value <= 0x0f) {
			return new RegisterIndirectValue(this, registerNames[value - 0x08]);
		} else if(value <= 0x17 && value < 0x20) {
			return new RamValue(this,
					(short)(getNextInstruction() + 
							getRegisterValue(registerNames[value])));
		} else if(value >= 0x20 && value <= 0x3f) {
			return new LiteralValue(this, (short)(value - 0x20));
		} else if (value >= 0x18 && value < 0x20) {
			switch(value) {
				case 0x18:
					return new StackValue(this);
				case 0x19:
					return new RamValue(this, getRegisterValue("SP"));
				case 0x1a:
					return new RamValue(this,
							(short)(getRegisterValue("SP") + getNextInstruction()));
				case 0x1b: 
					return new RegisterValue(this, "SP");
				case 0x1c:
					return new RegisterValue(this, "PC");
				case 0x1d:
					return new RegisterValue(this, "EX");
				case 0x1e:
					return new RamValue(this, getNextInstruction());
				case 0x1f:
					return new LiteralValue(this, getNextInstruction());
				default:
					return new LiteralValue(this, (short)0); // Never happens, just to avoid compiler complaining.
			}
		} else {
			return new LiteralValue(this, (short)0);
		}
	}
	
	/**
	 * Runs a single cycle of the emulator, running a single instruction.
	 * @return A StepResult object with details about the opcode that was run and the 
	 * changes that occured during this instruction.
	 * @throws InvalidOpCodeException If an invalid operation is encountered (usually
	 * as a result of a program attempting to execute data)
	 */
	public StepResult step() throws InvalidOpCodeException {
		displayChanged = false;
		changedRamLocations = new HashMap<Integer, Short>();
		short instruction = getNextInstruction();
		int opcode = instruction & OPCODE_MASK;

		int aValue = (instruction & A_MASK) >> A_SHIFT;
		int bValue = (instruction & B_MASK) >> B_SHIFT;
		DcpuValue a = getValue((short)aValue);
		DcpuValue b = getValue((short)bValue);

		DcpuCommand command;
		if(opcode != 0) {
			command = commands.get(opcode);
		} else {
			command = extendedCommands.get(bValue);
		}
		if(command == null) {
			throw new InvalidOpCodeException(
					"Invalid Operation: " + Integer.toBinaryString(opcode), -1);
		}
		
		// Run the command, and find out if it should skip further instructions
		// in case of a conditional that evaluated to false.
		boolean skip = command.run(a, b);
		if(skip) {
			skipInstructions();
		}
		return new StepResult(registers, changedRamLocations, opcode, aValue, bValue, displayChanged);
	}
	
	/**
	 * Skip instructions until after the next non-conditional instruction.
	 * e.g.
	 * 
	 * 1: IFE 4, A
	 * 2: IFG 5, B
	 * 3: MOV B, C
	 * 4: MOV X, Y
	 * 
	 * if this was called after running line 1, it would skip to line 4.
	 */
	public void skipInstructions() {
		boolean skip = true;
		while(skip) {
			short instruction = getNextInstruction();
			if(valueRequiresNextWord((instruction & A_MASK) >> A_SHIFT)) {
				getNextInstruction();
			}
			if(valueRequiresNextWord((instruction & B_MASK) >> B_SHIFT)) {
				getNextInstruction();
			}
			int opcode = instruction & OPCODE_MASK;
			if(!conditionalInstructions.contains(opcode)) {
				skip = false;
			}
		}
	}
	
	
	/**
	 * Check if a given value requires the next word to be read in.
	 * @param value The value to check.
	 * @return Whether or not the next word needs to be read for this value.
	 */
	private boolean valueRequiresNextWord(int value) {
		if(value >= 0x10 && value <= 0x17) { // register + next word
			return true;
		}
		if(value == 0x1a) { // pick n
			return true;
		}
		if(value == 0x1e) { // [next word]
			return true;
		}
		if(value == 0x1f) { // Literal next word
			return true;
		}
		return false;
	}
	
	/**
	 * Set up the DcpuCommand objects.
	 */
	public void loadCommands() {
		commands = new HashMap<Integer, DcpuCommand>();
		
		// These opcodes are from the dcpu spec: http://dcpu.com/dcpu-16/
		commands.put(0x01, new SetCommand());

		commands.put(0x02, new AddCommand(this));
		commands.put(0x03, new SubtractCommand(this));
		commands.put(0x04, new MultiplyCommand(this, false));
		commands.put(0x05, new MultiplyCommand(this, true));
		commands.put(0x06, new DivideCommand(this, true));
		commands.put(0x07, new DivideCommand(this, false));
		commands.put(0x08, new ModCommand(this, true));
		commands.put(0x09, new ModCommand(this, false));
		
		commands.put(0x0a, new LogicalCommand(this, LogicalOperation.AND));
		commands.put(0x0b, new LogicalCommand(this, LogicalOperation.OR));
		commands.put(0x0c, new LogicalCommand(this, LogicalOperation.XOR));
		commands.put(0x0d, new ShiftCommand(this, ShiftOperation.Right));
		commands.put(0x0e, new ShiftCommand(this, ShiftOperation.ArithmeticRight));
		commands.put(0x0f, new ShiftCommand(this, ShiftOperation.Left));
		
		commands.put(0x10, new ConditionalCommand(this, DcpuConditional.SharedBits));
		commands.put(0x11, new ConditionalCommand(this, DcpuConditional.NoSharedBits));
		commands.put(0x12, new ConditionalCommand(this, DcpuConditional.Equal));
		commands.put(0x13, new ConditionalCommand(this, DcpuConditional.NotEqual));
		commands.put(0x14, new ConditionalCommand(this, DcpuConditional.Greater));
		commands.put(0x15, new ConditionalCommand(this, DcpuConditional.GreaterUnsigned));
		commands.put(0x16, new ConditionalCommand(this, DcpuConditional.Less));
		commands.put(0x17, new ConditionalCommand(this, DcpuConditional.LessUnsigned));
		
		commands.put(0x1a, new AddExCommand(this));
		commands.put(0x1b, new SubExCommand(this));
		
		// These opcodes are actually in the b field, when the opcode field is set to 0
		extendedCommands = new HashMap<Integer, DcpuCommand>();
		extendedCommands.put(0x01, new JumpSetReturnCommand(this));
		extendedCommands.put(0x10, new HardwareNumCommand(this));
		extendedCommands.put(0x11, new HardwareQueryCommand(this));
	}
	
	/**
	 * Pop a value of the DCPU stack and return it. 
	 * @return The value popped off the stack.
	 */
	public short popStack() {
		short value = getValueAtRegisterValue("SP");
		incWrappedRegister("SP");
		return value;
	}
	
	/**
	 * Push a value onto the DCPU stack.
	 * @param value The value to push onto the stack.
	 */
	public void pushStack(short value) {
		decWrappedRegister("SP");
		setRamValue(getRegisterValue("SP"), value);
	}

}
