package commands.maths;

import utils.NumUtils;
import values.DcpuValue;

import commands.BaseCommand;
import commands.DcpuCommand;

import emulator.Emulator;
/**
 * Implements the DCPU subtract instruction.
 * 
 * SUB b, a
 * 
 * subtracts a from b, stores the result in b. If there is an underflow flow it stores 
 * 0xffff in EX, otherwise it stores 0 in EX.
 * 
 * @author Tony
 *
 */
public class SubtractCommand extends BaseCommand implements DcpuCommand {

	public SubtractCommand(Emulator dcpu) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		int aInt = NumUtils.unsignedShortToInt(a.getValue());
		int bInt = NumUtils.unsignedShortToInt(b.getValue());
		
		int result = bInt - aInt;
		if (result < 0) {
			result = 0xffff - result;
			dcpu.setRegisterValue("EX", (short)1);
		} else {
			dcpu.setRegisterValue("EX", (short)0);
		}
		b.setValue((short) (result));
		return false;
	}

}
