package commands.maths;

import utils.NumUtils;
import values.DcpuValue;

import commands.BaseCommand;
import commands.DcpuCommand;

import emulator.Emulator;

/**
 * Implements the MOD and MDI commands.
 * 
 * MOD b, a - signed modulus
 * MDI b, a - unsigned modulus
 * 
 * This does b % a, stores the first word of the result in b, and the rest in EX.
 * @author Tony
 *
 */
public class ModCommand extends BaseCommand implements DcpuCommand {
	
	private boolean signed;

	/**
	 * Creates a new ModCommand for handling modulus instructions. 
	 * @param dcpu A reference to the emulator.
	 * @param signed Whether to implement signed or unsigned modulus.
	 */
	public ModCommand(Emulator dcpu, boolean signed) {
		super(dcpu);
		this.signed = signed;
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		if(a.getValue() == 0) {
			b.setValue((short)0);
		} else {
			short aValue = a.getValue();
			short bValue = b.getValue();
			int aInt, bInt;
			if(!signed) {
				aInt = NumUtils.unsignedShortToInt(aValue);
				bInt = NumUtils.unsignedShortToInt(bValue);
			} else {
				aInt = aValue;
				bInt = bValue;
			}
			
			int result = bInt % aInt;
			b.setValue((short)result);
		}
		return false;
	}

}
