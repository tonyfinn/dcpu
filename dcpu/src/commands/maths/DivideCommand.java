package commands.maths;

import commands.BaseCommand;
import commands.DcpuCommand;

import utils.NumUtils;
import values.DcpuValue;
import emulator.Emulator;

/**
 * Implements the DIV and DVI commands.
 * 
 * DIV b, a - signed division
 * DVI b, a - unsigned division
 * 
 * This divides b by a, stores the first word of the result in b, and the rest in EX.
 * @author Tony
 *
 */
public class DivideCommand extends BaseCommand implements DcpuCommand {
	private boolean signed;

	/**
	 * Creates a new DivideCommand for handling divide instructions. 
	 * @param dcpu A reference to the emulator.
	 * @param signed Whether to implement signed or unsigned division.
	 */
	public DivideCommand(Emulator dcpu, boolean signed) {
		super(dcpu);
		this.signed = signed;
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		if(a.getValue() == 0) {
			dcpu.setRegisterValue("EX", (short)0);
			b.setValue((short)0);
		} else {
			// Treat a and b as unsigned integers
			short aValue = a.getValue();
			short bValue = b.getValue();
			
			int aInt, bInt;
			if(!signed) {
				aInt = NumUtils.unsignedShortToInt(aValue);
				bInt = NumUtils.unsignedShortToInt(bValue);
			} else {
				aInt = aValue;
				bInt = bValue;
			}
			
			// Multiply the results
			int result = bInt / aInt;
			int overflow = ((bInt << 16) / aInt) & 0xfff;
			
			// Set b to the lower 16 bits
			b.setValue((short)result);
			// Set EX to the overflow.
			dcpu.setRegisterValue("EX", (short)(overflow));
		}

		return false;
	}

}
