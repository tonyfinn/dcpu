package commands.maths;

import utils.NumUtils;
import values.DcpuValue;

import commands.BaseCommand;
import commands.DcpuCommand;
import emulator.Emulator;

/**
 * Implements the ADX command.
 * 
 * ADX b, a
 * 
 * adds b+a+EX and stores the result in b. If there's a overflow, it stores 1 in EX, otherwise
 * it stores 0 in EX.
 * @author Tony
 *
 */
public class AddExCommand extends BaseCommand implements DcpuCommand {

	public AddExCommand(Emulator dcpu) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		int aValue = NumUtils.unsignedShortToInt(a.getValue());
		int bValue = NumUtils.unsignedShortToInt(b.getValue());
		int exValue = NumUtils.unsignedShortToInt(dcpu.getRegisterValue("EX"));
		
		int result = aValue + bValue + exValue;
		if(result > 0xffff) {
			dcpu.setRegisterValue("EX", (short)1);
		} else {
			dcpu.setRegisterValue("EX", (short)0);
		}
		
		b.setValue((short)(result));
		return false;
	}

}
