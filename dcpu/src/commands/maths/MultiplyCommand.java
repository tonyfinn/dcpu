package commands.maths;

import commands.BaseCommand;
import commands.DcpuCommand;

import utils.NumUtils;
import values.DcpuValue;
import emulator.Emulator;

/**
 * Implements the MUL and MLI commands.
 * 
 * MUL b, a - signed multiplication
 * MLI b, a - unsigned multiplication
 * 
 * This multiplies b by a, stores the first word of the result in b, and the rest in EX.
 * @author Tony
 *
 */
public class MultiplyCommand extends BaseCommand implements DcpuCommand {
	
	private boolean signed;

	/**
	 * Creates a new MultiplyCommand for handling multiply instructions. 
	 * @param dcpu A reference to the emulator.
	 * @param signed Whether to implement signed or unsigned multiplication.
	 */
	public MultiplyCommand(Emulator dcpu, boolean signed) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		short aValue = a.getValue();
		short bValue = b.getValue();
		
		int aInt, bInt;
		if(!signed) {
			aInt = NumUtils.unsignedShortToInt(aValue);
			bInt = NumUtils.unsignedShortToInt(bValue);
		} else {
			aInt = aValue;
			bInt = bValue;
		}
		
		// Multiply the results
		int result = bInt * aInt;
		
		// Set b to the lower 16 bits
		b.setValue((short)result);
		// Set EX to the overflow.
		dcpu.setRegisterValue("EX", (short)((result >> 16) & 0xffff));
		return false;
	}

}
