package commands.maths;

import commands.BaseCommand;
import commands.DcpuCommand;
import emulator.Emulator;

import values.DcpuValue;

/**
 * Implements the DCPU add instruction.
 * 
 * ADD b, a
 * 
 * adds b and a, stores the result in b. If there is an overflow it stores 1 in EX, otherwise
 * it stores 0 in EX.
 * 
 * @author Tony
 *
 */
public class AddCommand extends BaseCommand implements DcpuCommand {

	public AddCommand(Emulator dcpu) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		int aValue = a.getValue();
		int bValue = b.getValue();
		int result = aValue + bValue;
		if(result > 0xffff) { // If overflow
			dcpu.setRegisterValue("EX", (short)1);
			result -= 0xffff;
		} else {
			dcpu.setRegisterValue("EX", (short)0);
		}
		b.setValue((short) (a.getValue() + b.getValue()));
		return false;
	}

}
