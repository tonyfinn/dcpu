package commands.maths;

import utils.NumUtils;
import values.DcpuValue;

import commands.BaseCommand;
import commands.DcpuCommand;
import emulator.Emulator;
/**
 * Implements the SBX command.
 * 
 * SBX b, a
 * 
 * calculates b + a - EX and stores the result in b. If there's a overflow, it stores 1 in EX,
 * if there's an underflow it stores 0xffff in EX, otherwise it stores 0 in EX.
 * @author Tony
 *
 */
public class SubExCommand extends BaseCommand implements DcpuCommand {

	public SubExCommand(Emulator dcpu) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		int aValue = NumUtils.unsignedShortToInt(a.getValue());
		int bValue = NumUtils.unsignedShortToInt(b.getValue());
		int exValue = NumUtils.unsignedShortToInt(dcpu.getRegisterValue("EX"));
		
		int result = aValue + bValue - exValue;
		if(result > 0xffff) {
			result -= 0xffff;
			dcpu.setRegisterValue("EX", (short)1);
		} else if (result < 0) {
			result += 0xffff;
			dcpu.setRegisterValue("EX", (short)0xffff);
		} else {
			dcpu.setRegisterValue("EX", (short)0);
		}
		b.setValue((short)result);
		return false;
	}

}
