package commands.conditional;

public enum DcpuConditional {
	Equal, NotEqual, Greater, GreaterUnsigned, Less, LessUnsigned, 
	SharedBits, // For IFB. Checks b&a != 0, i.e. if there are any bits 1 in both values
	NoSharedBits // Opposite of SharedBits
}
