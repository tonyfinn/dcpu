package commands.conditional;

import static utils.NumUtils.unsignedShortToInt;
import values.DcpuValue;

import commands.BaseCommand;

import emulator.Emulator;

/**
 * Represents all conditional commands within the DCPU, those are:
 * 
 * IFE b, a - Equality
 * IFN b, a - Inequality
 * IFG b, a - Greater than
 * IFA b, a - Signed Greater than
 * IFL b, a - Less than
 * IFU b, a - Signed Less than
 * IFB b, a - Bits in common
 * IFC b, a - No bits in common
 * 
 * If the comparison is false, all of these should skip all following conditional instructions
 * and the first non-conditional instruction.
 * 
 * @author Tony
 *
 */
public class ConditionalCommand extends BaseCommand {

	private DcpuConditional op;
	
	public ConditionalCommand(Emulator dcpu, DcpuConditional op) {
		super(dcpu);
		this.op = op;
	}
	
	public boolean isConditional() {
		return true;
	}
	
	public boolean run(DcpuValue a, DcpuValue b) {
		boolean result;
		switch(op) {
			case Equal:
				result = a.getValue() == b.getValue();
				break;
			case NotEqual:
				result = a.getValue() != b.getValue();
				break;
			case Greater:
				result = b.getValue() > a.getValue();
				break;
			case Less:
				result = b.getValue() < a.getValue();
				break;
			case GreaterUnsigned:
				result = unsignedShortToInt(b.getValue()) > unsignedShortToInt(a.getValue());
				break;
			case LessUnsigned:
				result = unsignedShortToInt(b.getValue()) < unsignedShortToInt(a.getValue());
				break;
			case SharedBits:
				result = (b.getValue() & a.getValue()) != 0;
				break;
			case NoSharedBits:
				result = (b.getValue() & a.getValue()) == 0;
				break;
			default:
				result = true;
				break;
		}
		return !result; // If the conditional returned true, the next instruction
						// should not be skipped.
	}

}
