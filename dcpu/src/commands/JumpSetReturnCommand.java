package commands;

import emulator.Emulator;
import values.DcpuValue;

/**
 * Implements the JSR command.
 * 
 * JSR a
 * 
 * pushs the program counter to the stack and jumps to the address specified by a.
 * 
 * @author Tony
 *
 */
public class JumpSetReturnCommand extends BaseCommand implements DcpuCommand {

	public JumpSetReturnCommand(Emulator dcpu) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		dcpu.pushStack((short)(dcpu.getRegisterValue("PC") + 1));
		dcpu.setRegisterValue("PC", a.getValue());
		return false;
	}

}
