package commands;

import values.DcpuValue;

/**
 * Implements the SET DCPU instruction.
 * SET b, a 
 * sets the value of B to the value of A.
 * 
 * @author Tony
 *
 */
public class SetCommand implements DcpuCommand {

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		b.setValue(a.getValue());
		return false;
	}

}
