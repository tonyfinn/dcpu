package commands.bitwise;

public enum LogicalOperation {
	AND, OR, XOR
}
