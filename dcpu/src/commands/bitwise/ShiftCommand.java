package commands.bitwise;

import values.DcpuValue;

import commands.BaseCommand;
import commands.DcpuCommand;
import emulator.Emulator;

/**
 * Implements all shift operations available within the DCPU. These are
 * 
 * ASR b, a  - Arithmetic Right Shift
 * SHR b, a  - Right Shift
 * SHL b, a  - Left Shift
 * 
 * All of these commands shift b by a, store the result in b, and store any overflow
 * in the EX register.
 * 
 * @author Tony
 *
 */
public class ShiftCommand extends BaseCommand implements
		DcpuCommand {

	private ShiftOperation op;

	/**
	 * Creates a new ShiftCommand
	 * @param dcpu The emulator
	 * @param op Which shift operation this ShiftCommand represents.
	 */
	public ShiftCommand(Emulator dcpu, ShiftOperation op) {
		super(dcpu);
		this.op = op;
	}
	
	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		int aValue = a.getValue();
		int bValue = b.getValue();
		
		int result, overflow;
		switch(op) {
			case ArithmeticRight:
				result = bValue >>> aValue;
				overflow = (bValue << 16) >> aValue; // Bits that were lost by shifting.
				break;
			case Right:
				result = bValue >> aValue;
				overflow = (bValue << 16) >>> aValue;
				break;
			case Left:
				result = bValue << aValue;
				overflow = (bValue << aValue) >> 16;
				break;
			default:
				// Should never be called
				result = 0xffff;
				overflow = 0xffff;
		}

		
		b.setValue((short)result);
		dcpu.setRegisterValue("EX", (short)(overflow & 0xffff));
		return false;
	}


}
