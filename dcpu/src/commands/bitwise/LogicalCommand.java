package commands.bitwise;

import values.DcpuValue;

import commands.BaseCommand;
import commands.DcpuCommand;

import emulator.Emulator;

/**
 * Implements all basic logical commands available in the DCPU instruction set:
 * 
 * AND b, a  - binary and
 * BOR b, a  - binary or
 * XOR b, a  - binary xor
 * 
 * Each of these commands performs the operation on b and a, then stores the result in b.
 * 
 * @author Tony
 *
 */
public class LogicalCommand extends BaseCommand implements DcpuCommand {
	
	private LogicalOperation op;

	public LogicalCommand(Emulator dcpu, LogicalOperation op) {
		super(dcpu);
		this.op = op;
	}
	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		int result;
		switch(op) {
		case AND:
			result = b.getValue() & a.getValue();
			break;
		case OR:
			result = b.getValue() | a.getValue();
			break;
		case XOR:
			result = b.getValue() ^ a.getValue();
			break;
		default:
			// Should never ever under any circumstances run.
			assert false;
			System.out.println("Someone added a new value.");
			result = 0xffff;
			break;
		}
		b.setValue((short) result);
		return false;
	}

}
