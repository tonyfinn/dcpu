package commands.bitwise;

public enum ShiftOperation {
	ArithmeticRight,
	Right,
	Left
}
