package commands.hardware;

import values.DcpuValue;
import commands.BaseCommand;
import commands.DcpuCommand;
import emulator.Emulator;

/**
 * Gets the number of connected hardware devices.
 * 
 * HWN a
 * 
 * stores the number of connected devices in a.
 * 
 * @author Tony
 *
 */
public class HardwareNumCommand extends BaseCommand implements DcpuCommand {

	public HardwareNumCommand(Emulator dcpu) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		a.setValue((short)dcpu.getHardware().size());
		return false;
	}

}
