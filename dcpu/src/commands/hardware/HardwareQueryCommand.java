package commands.hardware;

import java.util.List;

import utils.NumUtils;
import values.DcpuValue;

import commands.BaseCommand;
import commands.DcpuCommand;

import emulator.Emulator;

/**
 * Writes information about a specific hardware device.
 * 
 * HWQ a
 * 
 * writes information about device #a to the A, B, C, X and Y registers.
 * 
 * @author Tony
 *
 */
public class HardwareQueryCommand extends BaseCommand implements DcpuCommand {

	public HardwareQueryCommand(Emulator dcpu) {
		super(dcpu);
	}

	@Override
	public boolean run(DcpuValue a, DcpuValue b) {
		int hardwareNum = NumUtils.unsignedShortToInt(a.getValue());
		List<HardwareDevice> allHardware  = dcpu.getHardware();
		int manufacturerID, deviceID;
		short version;
		if(hardwareNum >= allHardware.size()) {
			manufacturerID = deviceID = version = (short) 0;
		} else {
			HardwareDevice hw = allHardware.get(hardwareNum);
			manufacturerID = hw.getManufacturerID();
			deviceID = hw.getDeviceID();
			version = hw.getVersion();
		}
		dcpu.setRegisterValue("A", NumUtils.getLowBits(deviceID));
		dcpu.setRegisterValue("B", NumUtils.getHighBits(deviceID));
		dcpu.setRegisterValue("C", version);
		dcpu.setRegisterValue("X", NumUtils.getLowBits(manufacturerID));
		dcpu.setRegisterValue("Y", NumUtils.getHighBits(manufacturerID));
		return false;
	}

}
