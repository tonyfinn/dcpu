package commands.hardware;

public class HardwareDevice {
	private int manufacturerID;
	private int deviceID;
	private short version;
	
	public HardwareDevice(int manufacturerID,
			int deviceID, short version) {
		this.manufacturerID = manufacturerID;
		this.deviceID = deviceID;
		this.version = version;
	}
	
	public int getManufacturerID() {
		return manufacturerID;
	}
	public int getDeviceID() {
		return deviceID;
	}
	public short getVersion() {
		return version;
	}
}
