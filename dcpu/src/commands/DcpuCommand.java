package commands;

import values.DcpuValue;

/**
 * The DCPU command interface is used by all implementions of DCPU instructions. 
 * It contains only a single method, run(DcpuValue a, DcpuValue b) which runs 
 * the specified instruction with b and a parameters as represented by the DcpuValue 
 * objects. Different concrete subclasses represent different implemented instructions.
 * @author Tony Finn and Samuel Haycock
 */
public interface DcpuCommand {
	/**
	 * Runs the specified instruction.
	 * @param a The parameter in the A position on the instruction.
	 * @param b The parameter in the B position on the instruction.
	 * @return Whether or not to skip instructions until after the 
	 * next non-conditional instruction.
	 */
	public boolean run(DcpuValue a, DcpuValue b);
}
