package commands;

import emulator.Emulator;

/**
 * This class is used by most implementations of DCPU instructions. It provides a 
 * shared way to access other elements of the DCPU apart from the A and B values 
 * that were passed to the instruction. The reason this is necessary is that many 
 * commands need to change additional details such as pushing a value to the stack in 
 * the case of the JSR instruction (implemented in the JumpSetReturnCommand class), or 
 * setting the EX register in the event of an overflow for the ADD instruction 
 * (implemented in the AddCommand class).
 * 
 * @author Tony
 *
 */
public abstract class BaseCommand implements DcpuCommand {
	protected Emulator dcpu;
	
	/**
	 * Create a new instance of a commmand with a reference to the emulator.
	 * @param dcpu A instance of the emulator.
	 */
	public BaseCommand(Emulator dcpu) {
		this.dcpu = dcpu;
	}
	
	/**
	 * Check whether or not a command is a conditional.
	 * @return
	 */
	public boolean isConditional() {
		return false;
	}

}
