package values;

import emulator.Emulator;

/**
 * Represents a value stored in a register. 
 * 
 * Getting/Setting the value read or write to the register respectively.
 * @author Tony
 *
 */
public class RegisterValue implements DcpuValue {
	
	private String name;
	private Emulator dcpu;

	public RegisterValue(Emulator dcpu, String name) {
		this.dcpu = dcpu;
		this.name = name;
	}
	@Override
	public short getValue() {
		return dcpu.getRegisterValue(name);
	}

	@Override
	public void setValue(short value) {
		dcpu.setRegisterValue(name, value);
	}

}
