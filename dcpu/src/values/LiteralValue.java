package values;

import emulator.Emulator;

/**
 * Represents a literal number in the program. Getting the value returns the number,
 * setting it fails and logs it to System.out.
 * @author Tony
 */
public class LiteralValue implements DcpuValue {

	private short value;
	
	
	/**
	 * @param dcpu Ignored, parameter only exists for consistency with
	 * 				other constructors.
	 * @param value
	 */
	public LiteralValue(Emulator dcpu, short value) {
		this.value = value;
	}
	
	@Override
	public short getValue() {
		return value;
	}

	@Override
	public void setValue(short value) {
		// Spec says this should fail silently but log it anyway.
		System.out.println("Attempt to write to literal value");
	}

}
