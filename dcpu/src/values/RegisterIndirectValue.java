package values;

import emulator.Emulator;

/**
 * Represents a value which is stored in RAM, where the RAM address is stored
 * in a specified register.
 * 
 * Getting the value reads the address from the register then reads the value from
 * that address of RAM.
 * 
 * Setting the value reads the address from the register then writes to that location
 * in RAM.
 * @author Tony
 *
 */
public class RegisterIndirectValue implements DcpuValue {
	
	private String name;
	private Emulator dcpu;

	public RegisterIndirectValue(Emulator dcpu, String name) {
		this.dcpu = dcpu;
		this.name = name;
	}
	
	@Override
	public short getValue() {
		return dcpu.getValueAtRegisterValue(name);
	}

	@Override
	public void setValue(short value) {
		dcpu.setRamValue(dcpu.getRegisterValue(name), value);
	}

}
