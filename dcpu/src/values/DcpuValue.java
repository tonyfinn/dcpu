package values;

/**
 * DcpuValue is an interface for an object which represents a parameter to a DCPU 
 * instruction. Because instructions can take many types of values - literals, 
 * registers, RAM addresses - and the different commands don�t need to know the 
 * details of which type of value they were passed. It provides two methods - 
 * getValue() which returns a short that represents the numeric value for use in the 
 * instruction, and setValue(short x) which sets the value. Different concrete 
 * subclasses then handle the actual implementation of these methods by doing things 
 * such as writing to the DCPU stack, setting register values, modifying RAM locations, etc.
 * @author Tony
 *
 */
public interface DcpuValue {
	/**
	 * Gets the value  of this parameter.
	 * @return The value to use for calculations.
	 */
	public short getValue();
	/**
	 * Set the value for this parameter.
	 * @param value The value to set.
	 */
	public void setValue(short value);
}
