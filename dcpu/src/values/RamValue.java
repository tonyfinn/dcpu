package values;

import emulator.Emulator;

/**
 * Represents a value which is a RAM address. Getting the value returns the value stored 
 * in the RAM at that address, while setting it writes to the RAM location.
 * @author Tony
 *
 */
public class RamValue implements DcpuValue {

	private short address;
	private Emulator dcpu;
	
	public RamValue(Emulator dcpu, short address) {
		this.dcpu = dcpu;
		this.address = address;
	}
	
	@Override
	public short getValue() {
		return dcpu.getRamValue(address);
	}

	@Override
	public void setValue(short value) {
		dcpu.setRamValue(address, value);
	}

}
