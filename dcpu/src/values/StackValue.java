package values;

import emulator.Emulator;

/**
 * Represents a value stored on the DCPU's stack. 
 * 
 * Getting the value pops it off the stack, while writing the value pushes it onto the stack.
 * @author Tony
 *
 */
public class StackValue implements DcpuValue {
	
	private Emulator dcpu;
	
	public StackValue(Emulator dcpu) {
		this.dcpu = dcpu;
	}
	
	@Override
	public short getValue() {
		return dcpu.popStack();
	}

	@Override
	public void setValue(short value) {
		dcpu.pushStack(value);
	}

}
